### Ad hoc On Demand Multipath Routing with Lifetime Maximization (AOMR-LM)

AOMR-LM is a multipath routing protocol based on AOMDV routing protocol. This protocol checks energy level of each nodes in the network and chooses a route based on energy levels of each nodes forming the route. The idea is to balances the energy consumption and avoid link failure from energy issue, thus maximize the lifetime of the nodes in the network.

### Modification

There are some modified files

1. `aomdv.h` file

- Add node object `*iNode` and new energy level variable `iEnergy` to get energy in `class AOMDVBroadcastID`

```
public:
    double iEnergy;
    MobileNode *iNode;
```

- Add two variables `rq_min_life` and `rq_cost` in request header `struct hdr_aomdv_request`

```
double rq_min_life;
double rq_cost;
```

- Add two variables `rp_cost` in reply header `struct hdr_aomdv_reply`

```
double rp_cost;
```

2. `aomdv.cc` file

- Initialize `iNode` and `iEnergy` variable when starting the node (in `AOMDV::AOMDV` constructor)

```
MobileNode *iNode;
iEnergy = 1.0;
```

- Store current node energy to a variable in `recvRequest` function and `recvReply` function

```
iNode = (MobileNode *) (Node::get_node_by_address(index));
xpos = iNode->X();
ypos = iNode->Y();
iEnergy = iNode->energy_model()->energy();
```

- Set energy level to the request header `rq->rq_min_life` in `recvRequest` function

```
if (iEnergy < rq->rq_min_life && iEnergy > 0) {
    rq->rq_min_life = iEnergy;
}
```

- To pass the route energy cost value to the next node, set `rq->rq_cost = rq->rq_min_life/rq->rq_hop_count` in `recvRequest` function

```
if (rq->rq_hop_count == 0) {
    rq->rq_cost = 0;
} else {
    rq->rq_cost = rq->rq_min_life/rq->rq_hop_count;
}
```

- To search address and broadcast ID pair and remove the high cost nodes, we modified the search algorithm in `id_get` function

```
for ( ; b; b = b->link.le_next) {
    if ((b->src == id) && (b->id == bid)) {
        if (!check_cost || cost <= b->cost) return b;
        id_delete(id, bid, b->cost);
    }
}
```
